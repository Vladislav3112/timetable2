package com.example.victor.notesample;

/**
 * Created by витя on 07.05.2017.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Timepicker extends AppCompatActivity {
    private static final int NOTIFY_ID = 101;
    public ArrayList<String> TaskTime;
    private android.widget.TimePicker mTimePicker;
    private NoteAdapter adapter;
    private EditorActivity editorActivity;

    public Timepicker (){TaskTime = new ArrayList<>();}





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void checkNotification(){
        editorActivity = new EditorActivity();
        TaskTime.add(editorActivity.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (simpleDateFormat.format(new Date()) != TaskTime.get(TaskTime.size()-1)) {
        if (simpleDateFormat.format(new Date()) == TaskTime.get(TaskTime.size()-1)) {
            CreateNotify();
        }
        }
    }
    public void TaskDeleted(int position) {
        TaskTime.remove(position);
    }


    public void CreateNotify() {

        Context context = getApplicationContext();
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
// оставим только самое необходимое
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentTitle("Напоминание")
                .setContentText("Задача скоро начинается!");

        Notification notification = builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIFY_ID, notification);
    }


    public void monChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 2))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 2)) {
                CreateNotify();
            }
        }
    }

    public void tueChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 3))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 3)) {
                CreateNotify();
            }
        }
    }


    public void wenChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 4))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 4)) {
                CreateNotify();
            }
        }
    }

    public void thurChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);

        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 5))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 5)) {
                CreateNotify();
            }
        }
    }

    public void friChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 6))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 6)) {
                CreateNotify();

            }
        }
    }

    public void satChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 7))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 7)) {
                CreateNotify();
            }
        }
    }

    public void sunChecked(int position) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        while (!((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 3))) {
            if ((simpleDateFormat.format(new Date()) == TaskTime.get(position)) & (dayOfWeek == 1)) {
                CreateNotify();

            }
        }
    }
}

