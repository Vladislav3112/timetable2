package com.example.victor.notesample;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.example.victor.notesample.DBHelper.TABLE_NAME;

/**
 * Created by victor on 17.02.17.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    private DBHelper db;
    private ArrayList<Note> mNotes;
    private Context mContext;
    private String s="00:00";
    private Timepicker timepicker;
    private Button btnDeleteTask;
    public int rowId;
    private int hour;
    private EditorActivity editorActivity;
    private int minunes;
    private int pos;

    public NoteAdapter (Context context) {
        this.mContext = context;
    }


    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_note_item,
                parent, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {

        Note note = mNotes.get(position);
        holder.mTVTitle.setText(note.getTitle());
        Date date = new Date(note.getDate());
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        StringBuilder builder = new StringBuilder();
        builder.append(cal.get(Calendar.HOUR))
                .append(":")
                .append(cal.get(Calendar.MINUTE))
                .append(" ")
                .append(cal.get(Calendar.DAY_OF_MONTH))
                .append(" ")
                .append(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        holder.mTVDate.setText(builder.toString());


    }
    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public void setmNotes(ArrayList<Note> notes) {
        this.mNotes = notes;
    }

    public void setTVtime() {
        editorActivity=new EditorActivity();
        s=editorActivity.getTime();
    }


    class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mTVTitle, mTVDate,mTVTime;
        public NoteViewHolder(View itemView) {
            super(itemView);
            mTVTime = (TextView) itemView.findViewById(R.id.task_time);
            mTVTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mTVDate = (TextView) itemView.findViewById(R.id.tv_date);
            btnDeleteTask = (Button) itemView.findViewById(R.id.btn_delete);
            mTVTime.setText(s);





            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EditorActivity editorActivity=new EditorActivity();
                    pos=editorActivity.GetPosition();
                    if (view.getId()==R.id.btn_delete){int rowId = getAdapterPosition();
                        db.DeleteTask(rowId);
                        Timepicker timepicker=new Timepicker();
                        timepicker.TaskDeleted(getAdapterPosition());
                        editorActivity.setPosition(pos-1);
                    }
                }

            });

        }


        @Override
        public void onClick(View view) { // нажатие на заметку
                Intent intent = new Intent(mContext, EditorActivity.class);
                // передается id заметки в новую Активность
                intent.putExtra(EditorActivity.INTENT_KEY_NOTE_ID, mNotes.get(getAdapterPosition()).getId());
                mContext.startActivity(intent);




        }

    }


}
