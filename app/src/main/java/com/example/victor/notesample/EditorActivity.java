package com.example.victor.notesample;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EditorActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String INTENT_KEY_NOTE_ID = "note_id";
    private DBHelper mDBHelper;
    private Note mNote;
    private String str;
    public SimpleDateFormat simpleDateFormat;
    private Button btnTime,btnChangeTime;
    private FloatingActionButton btnAddNote;
    public CheckBox CBmon,CBtue,CBwen,CBthur,CBfri,CBsut,CBsun;
    private EditText mETTitle, mETDescription,mETtaskTime;
    private Timepicker timepicker;
    private NoteAdapter adapter;
    public String time="00:00";
    public int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        mETtaskTime = (EditText) findViewById(R.id.et_task_time);
        mETDescription = (EditText) findViewById(R.id.tv_note_text);
        mETTitle = (EditText) findViewById(R.id.tv_title);


        btnAddNote = (FloatingActionButton) findViewById(R.id.b_add_note);
        btnAddNote.setOnClickListener(this);
        btnChangeTime = (Button) findViewById(R.id.btn_change_time);
        btnChangeTime.setOnClickListener(this);

        CBmon = (CheckBox) findViewById(R.id.ch_box_1_st_day);
        CBtue = (CheckBox) findViewById(R.id.ch_box_2_nd_day);
        CBwen = (CheckBox) findViewById(R.id.ch_box_3_rd_day);
        CBthur = (CheckBox) findViewById(R.id.ch_box_4_th_day);
        CBfri = (CheckBox) findViewById(R.id.ch_box_5_th_day);
        CBsut = (CheckBox) findViewById(R.id.ch_box_6_th_day);
        CBsun = (CheckBox) findViewById(R.id.ch_box_7_th_day);

        if (CBmon.isChecked()) {
            position=adapter.getItemCount();
            timepicker.monChecked(position);
        }
        if (CBtue.isChecked()) {
            position=adapter.getItemCount();
            timepicker.tueChecked(position);
        }
        if (CBwen.isChecked()) {
            position=adapter.getItemCount();
            timepicker.wenChecked(position);
        }
        if (CBthur.isChecked()) {
            position=adapter.getItemCount();
            timepicker.thurChecked(position);
        }
        if (CBfri.isChecked()) {
            position=adapter.getItemCount();
            timepicker.friChecked(position);
        }
        if (CBsut.isChecked()) {
            position=adapter.getItemCount();
            timepicker.satChecked(position);
        }
        if (CBsun.isChecked()) {
            position=adapter.getItemCount();
            timepicker.sunChecked(position);
        }
        mDBHelper = new DBHelper(this);
        int id = getIntent().getIntExtra(INTENT_KEY_NOTE_ID, -1);


    }
    public int GetPosition(){
        return position;
    }
    public void setPosition(int position){
        this.position=position;
    }
    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Новая заметка");
            setTitle("Новая заметка");
        }
    }

    public String  getTime(){
        return time;
    }
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.b_add_note){
            time=mETtaskTime.getText().toString();
            adapter=new NoteAdapter(this);
            adapter.setTVtime();

            String title = mETTitle.getText().toString();
            Date date = new Date();
            Note note = new Note(title,date.getTime());
            mDBHelper.createNote(note);
            Intent intent = new Intent(EditorActivity.this, MainActivity.class);

            startActivity(intent);
            Log.d("Mylog","intent on main activity");
            timepicker=new Timepicker();
            timepicker.checkNotification();
        }
    }
}
